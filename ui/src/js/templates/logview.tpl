<h1 class="page-title">
    <i class="fa fa-th-list"></i>
    {{project}} - {{logid}}
</h1>

<div class="content-wrap">
	<h3>{{timestamp}}</h3>
	<pre data-log="{{logid}}">{{{file}}}</pre>
	<div id="log-bottom"></div>
</div>